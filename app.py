from flask import Flask, render_template, request,make_response
import json
from training.countSentiment import count_sentimen
from mongo import crud
from wtforms import Form, TextAreaField,validators
from preprocessing import preprocess
from training import training


app = Flask(__name__)


class KlasifikasiForm(Form):
    data = TextAreaField('',[validators.DataRequired])


@app.route('/')
def index():
    labels = ["Positif", "Negatif", "Netral"]
    colors = ["#F7464A", "#46BFBD", "#FDB45C"]
    values = crud.getCountAll()
    # values = [10,10,10]
    return render_template('index.html', values=json.dumps(values), colors=json.dumps(colors),
                           labels=json.dumps(labels))

@app.route('/chart')
def chart():
    labels = ["Positif","Negatif","Netral"]
    colors = ["#F7464A", "#46BFBD", "#FDB45C"]
    values = count_sentimen()
    # values = [10,10,10]
    return render_template('chart.html',values= json.dumps(values), colors = json.dumps(colors), labels = json.dumps(labels))

@app.route('/klasifikasi')
def klasifikasi():
    form = KlasifikasiForm(request.form)
    return render_template('form_klasifikasi.html',form=form)
@app.route('/hasil',methods=['GET','POST'])
def hasil():
    sentimen = None
    if request.method == 'POST':
        # tweet = request.get_data()
        tweet = request.get_json()
        tweet, tweet_pre = preprocess.preprocess(tweet)
        print(type(tweet))
        resp = {}
        resp['sentimen']=(training.prediksi(tweet))
        resp['tweet_pre']=(tweet_pre)
        sentimen = make_response(json.dumps(resp))
    return sentimen



if __name__ == '__main__':
    app.run()
