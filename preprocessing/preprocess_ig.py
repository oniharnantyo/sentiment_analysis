import re
from Sastrawi.StopWordRemover.StopWordRemoverFactory import StopWordRemoverFactory
def preprocess(teks):
    hasil = re.sub('(www|http|pic|https)\S+', '', teks, flags=re.MULTILINE).lower();
    # print('hapus url: ', hasil)
    # hapus username
    hasil = re.sub('@[^\s]+', '', hasil, flags=re.MULTILINE)
    # print('hapus username: ', hasil)
    # hapus tanda
    hasil = re.sub('[^A-Za-z09\\s]+', '', hasil, flags=re.MULTILINE)
    # print('hapus tanda: ', hasil)
    # hapus pagar
    hasil = re.sub('#[^\s]+', '', hasil, flags=re.MULTILINE)
    # print('hapus pagar: ', hasil)
    # hapus angka
    hasil = re.sub('([09]+)(\\s|$)', '', hasil, flags=re.MULTILINE)
    hasil = re.sub('(^|\\s)([09]+)', '', hasil, flags=re.MULTILINE)
    # print('hapus angka: ', hasil)
    factory = StopWordRemoverFactory()
    stopword = factory.create_stop_word_remover()
    hasil = stopword.remove(hasil)
    return hasil