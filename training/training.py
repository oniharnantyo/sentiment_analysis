import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
from keras.preprocessing.sequence import pad_sequences
from keras.models import Sequential
from keras.layers import Dense, Embedding, LSTM, SpatialDropout1D
from keras.models import model_from_json
from sklearn.model_selection import train_test_split
from keras.preprocessing.text import Tokenizer
import pickle
import tensorflow as tf
from mongo import crud
from time import time
from keras.callbacks import TensorBoard
from keras.backend.tensorflow_backend import set_session
from preprocessing import preprocess


config = tf.ConfigProto()
config.gpu_options.allow_growth = True  # dynamically grow the memory used on the GPU
config.log_device_placement = False  # to log device placement (on which device the operation ran)
# (nothing gets printed in Jupyter, only if you run it standalone)
sess = tf.Session(config=config)
set_session(sess)  # set this TensorFlow session as the default session for Keras


def train():
    # data = pd.read_csv(file,delimiter=',')
    # data = data[['text', 'sentimen']]
    # data = data[data.sentiment != 0]
    data = pd.DataFrame(list(crud.getAll()))
    data = data[['text', 'sentimen']]

    # data['text'] = data['text'].apply(lambda x: x.lower())
    # data['text'] = data['text'].apply((lambda x: re.sub('[^a-zA-z0-9\s]', '', x)))

    # data['text'] = data['text'].apply(lambda x: preprocess(x))
    # data.to_csv('hasil.csv',sep=',', encoding='utf-8');
    print(data[data['sentimen'] == 0].size)
    print(data[data['sentimen'] == 1].size)
    print(data[data['sentimen'] == 2].size)

    # for idx, row in data.iterrows():
    #     row[0] = row[0].replace('rt', ' ')

    max_fatures = 2000
    tokenizer = Tokenizer(num_words=max_fatures, split=' ')
    tokenizer.fit_on_texts(data['text'].values)
    X = tokenizer.texts_to_sequences(data['text'].values)
    X = pad_sequences(X)

    with open('tokenizer.pickle', 'wb') as handle:
        pickle.dump(tokenizer, handle, protocol=pickle.HIGHEST_PROTOCOL)

    embed_dim = 128
    lstm_out = 196

    # embed_dim = 256
    # lstm_out = 392

    model = Sequential()
    model.add(Embedding(max_fatures, embed_dim, input_length=X.shape[1]))
    model.add(SpatialDropout1D(0.4))
    model.add(LSTM(lstm_out, dropout=0.2, recurrent_dropout=0.2))
    model.add(Dense(3, activation='softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    tensorboard = TensorBoard(log_dir="logs/{}".format(time()))
    print(model.summary())

    Y = pd.get_dummies(data['sentimen']).values
    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.30, random_state=42)
    print(X_train.shape, Y_train.shape)
    print(X_test.shape, Y_test.shape)

    batch_size = 32
    model.fit(X_train, Y_train, epochs=20, batch_size=batch_size, verbose=2,callbacks=[tensorboard])

    model_json = model.to_json()
    with open("model.json","w") as json_file:
        json_file.write(model_json)
    model.save_weights("model.h5")
    print("Model saved to disk")

    # load json and create model
    json_file = open('model.json', 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)
    # load weights into new model
    loaded_model.load_weights("model.h5")
    print("Loaded model from disk")

    validation_size = 1000

    X_validate = X_test[-validation_size:]
    Y_validate = Y_test[-validation_size:]
    X_test = X_test[:-validation_size]
    Y_test = Y_test[:-validation_size]
    score, acc = model.evaluate(X_test, Y_test, verbose=2, batch_size=batch_size)
    print("score: %.2f" % (score))
    print("acc: %.2f" % (acc))

    pos_cnt, neg_cnt, net_cnt, pos_correct, neg_correct,net_correct = 0, 0, 0, 0,0,0
    for x in range(len(X_validate)):

        result = model.predict(X_validate[x].reshape(1, X_test.shape[1]), batch_size=1, verbose=2)[0]

        if np.argmax(result) == np.argmax(Y_validate[x]):
            if np.argmax(Y_validate[x]) == 0:
                neg_correct += 1
            else:
                pos_correct += 1

        if np.argmax(Y_validate[x]) == 0:
            neg_cnt += 1
        else:
            pos_cnt += 1

    print("pos_acc", pos_correct / pos_cnt * 100, "%")
    print("neg_acc", neg_correct / neg_cnt * 100, "%")

def prediksi(teks):
    teks = [teks]
    # loading
    with open('G:/project/Skripsi/training/tokenizer.pickle', 'rb') as handle:
        tokenizer = pickle.load(handle)

    # load json and create model
    json_file = open('G:/project/Skripsi/training/model.json', 'r')
    loaded_model_json = json_file.read()
    json_file.close()

    # import tensorflow as tf
    # from keras.backend.tensorflow_backend import set_session
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True  # dynamically grow the memory used on the GPU
    config.log_device_placement = False  # to log device placement (on which device the operation ran)
    # (nothing gets printed in Jupyter, only if you run it standalone)
    sess = tf.Session(config=config)
    set_session(sess)  # set this TensorFlow session as the default session for Keras
    loaded_model = model_from_json(loaded_model_json)
    # load weights into new model
    loaded_model.load_weights("G:/project/Skripsi/training/model.h5")
    twt = tokenizer.texts_to_sequences(teks)
    # padding the tweet to have exactly the same shape as `embedding_2` input
    twt = pad_sequences(twt, maxlen=37, dtype='int32', value=0)
    # print(twt)
    print("twt: ",twt)
    sentiment = loaded_model.predict(twt, batch_size=1, verbose=2)[0]
    print(np.argmax(sentiment))
    if (np.argmax(sentiment) == 0):
        return ("NEGATIF")
    elif (np.argmax(sentiment) == 1):
        return ("NETRAL")
    elif (np.argmax(sentiment) == 2):
        return ("POSITIVE")


# train()
# # # train('1.csv')
# print('hasil: ',prediksi(['Harganya mahal pake banget! Ke Jogja gak pernah mau makan lesehan Malioboro, dah sering denger ']))
# print('hasil: ',prediksi(['Udah dari kapan tahun kali ya warung lesehan mahal dan ga enak di Malioboro ini mendingan ke yg jelas2 macam Raminten  ']))
#
# print('hasil: ',prediksi(['jalan mangkubumi parkiran yang luas buat hotel hotel yang menjamu']))
# print('hasil: ',prediksi(['Baru saja mengirim foto']))
#
# print('hasil: ',prediksi(['Yang saya suka dari Malioboro hari selasa wage, bersih, lenggang, ga bau pesing, nyaman jalan di sepanjang boulevard']))
# print('hasil: ',prediksi(['Pedestrian Malioboro Sudah Dipercantik, Tapi Malah Muncul PKL dan Parkiran Liar']))
# print('hasil: ',prediksi('Yang saya suka dari Malioboro hari selasa wage, bersih, lenggang, ga bau pesing, nyaman jalan di sepanjang boulevard'))



