from gensim.models import Word2Vec
import pandas as pd
import re
from tqdm import tqdm
from gensim.models import Phrases
from gensim.models.phrases import Phraser
import random
from preprocessing.preprocess_ig import  preprocess


def read_csv(data):
    df = pd.read_csv(data)
    print(df['teks'])

def clean(sentence):
    sentence = re.sub(r'[^A-Za-z0-9\s.]', r'', str(sentence).lower())
    sentence = re.sub(r'\n', r' ', sentence)

    return sentence

def make_w2v(data):
    df = pd.read_csv(data)
    df['text'] = df['text'].map(lambda x: preprocess(x))
    tmp_corpus = df['text'].map(lambda x: x.split(' '))
    corpus = []
    for i in tqdm(range(len(tmp_corpus))):
        for line in tmp_corpus[i]:
            words = [x for x in line.split()]
            corpus.append(words)
    num_of_sentences = len(corpus)
    num_of_words = 0
    for line in corpus:
        num_of_words += len(line)

    print('Num of sentences - %s' % (num_of_sentences))
    print('Num of words - %s' % (num_of_words))
    phrases = Phrases(sentences=corpus, min_count=25, threshold=50)
    bigram = Phraser(phrases)

    for index, sentence in enumerate(corpus):
        corpus[index] = bigram[sentence]

    # shuffle corpus
    def shuffle_corpus(sentences):
        shuffled = list(sentences)
        random.shuffle(shuffled)
        return shuffled

    model = Word2Vec(
        corpus,
        size=250,
        window=10,
        min_count=2,
        workers=10)
    model.wv.save_word2vec_format("w2v_model")

def transform(data):
    model = Word2Vec.wv.load_word2vec_format("w2v_model")
    vector = model.wv[data]
    return vector
def most_similar(data):
    model = Word2Vec.wv.load_word2vec_format("w2v_model")
    # print()model.vocabulary()
    return model.most_similar(data)

# make_w2v('data-malioboro.csv')

#make_w2v('data-malioboro.csv')
# print(most_similar('malioboro'))

import re
with open('w2v_model','r') as file:
    with open('w2v_model.txt','w') as file2:
        for data in file.readlines():
            file2.writelines(",".join(data.split(" ", 1)))

        # data = [",".join(line.split(" ", 1)) for line in data]
        # print(data)
