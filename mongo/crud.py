from mongo import koneksi

koneksi1 = koneksi.getKoneksi()
col = koneksi1.dataset

def insert(record):
    col.insert(record)
def getAll():
    return col.find({});
def getCountAll():
    data = []
    data.append(col.find({"sentimen": 2}).count())
    data.append(col.find({"sentimen": 0}).count())
    data.append(col.find({"sentimen": 1}).count())
    return data
def getCount(tag):
    data = []
    data.append(col.find({"sentimen": 2,"tag": tag}).count())
    data.append(col.find({"sentimen": 0,"tag": tag}).count())
    data.append(col.find({"sentimen": 1,"tag": tag}).count())
    return data



# print(getCount("malioboro"))

# data = []
# for mongo in getAll():
#     row = []
#     row.append(mongo['text'])
#     row.append(mongo['sentimen'])
#     data.append(row)
#
# print(data[0])


import pandas as pd
# data = pd.read_excel('../training/datafix.xlsx')
# data = data[['text', 'sentimen']]
# print(data)

# data = pd.DataFrame(list(getAll()))
# data = data[['text', 'sentimen']]
# print(data)

